import cv2
import numpy as np

    
def drawBoundingBox(img):

    default_img = img.copy()
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    def append_to_boundingRect(br1, br2):
        """ Append to boundingbox to one. """
        x1, y1, w1, h1 = br1
        x2, y2, w2, h2 = br2
        height = 2*min(h1, h2) + max(h1, h2)
        return min(x1, x2), min(y1, y2), max(w1, w2), height

    def draw_bounding_rect(bounding_rect):
        """ Draw boundingbox on image. """
        x, y, w, h = bounding_rect
        cv2.rectangle(default_img, (x, y), (x+w, y+h), (255, 0, 0))

    _, threshold = cv2.threshold(img, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)

    contours, _ = cv2.findContours(threshold, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    contours = sorted(contours, key=lambda ctr: cv2.boundingRect(ctr)[0])

    bounding_rects = [cv2.boundingRect(ctr) for ctr in contours]

    # threshold for x coordinate differnce
    x_coo_difference_threshold = np.average(np.abs(np.diff([r[0] + 0.5*r[2] for r in bounding_rects])))/4
    
    len_br = len(bounding_rects)-1
    i = 0
    while i < len_br:
        # Get bounding box
        br1 = bounding_rects[i]
        br2 = bounding_rects[i+1]
        # check boundingbox position different, detect i characters, and add them together
        if abs((br1[0] + 0.5*br1[2])-(br2[0] + 0.5*br2[2])) < x_coo_difference_threshold:
            x, y, w, h = append_to_boundingRect(br1, br2)
            del bounding_rects[i+1]
            bounding_rects[i] = (x, y, w, h)
            len_br-=1
            
        # draw charater boundingbox on the final image
        draw_bounding_rect(bounding_rects[i])
        i += 1
    
    try:
        draw_bounding_rect(bounding_rects[-1])
    except IndexError:
        pass
    cv2.rectangle(default_img, (0, 0), (img.shape[1]-1, img.shape[0]-1), (0, 0, 255), thickness=2)

    return default_img
