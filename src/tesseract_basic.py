import re
import cv2
import pytesseract
from pytesseract import Output
import numpy as np

pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files/Tesseract-OCR/tesseract.exe'


class MyTesseract:
    # get grayscale image
    def get_grayscale(image):
        return cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

    # noise removal

    def remove_noise(image):
        return cv2.medianBlur(image, 5)

    # thresholding

    def thresholding(image):
        return cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

    # dilation

    def dilate(image):
        kernel = np.ones((5, 5), np.uint8)
        return cv2.dilate(image, kernel, iterations=1)

    # erosion

    def erode(image):
        kernel = np.ones((5, 5), np.uint8)
        return cv2.erode(image, kernel, iterations=1)

    # opening - erosion followed by dilation

    def opening(image):
        kernel = np.ones((5, 5), np.uint8)
        return cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)

    # canny edge detection

    def canny(image):
        return cv2.Canny(image, 100, 200)

    def RecLine(image):
        d = pytesseract.image_to_data(image, output_type=Output.DICT)
        print(d.keys())
        n_boxes = len(d['text'])
        for i in range(n_boxes):
            print(d['conf'][i])
            if float(d['conf'][i]) > 60:
                (x, y, w, h) = (d['left'][i], d['top']
                                [i], d['width'][i], d['height'][i])
                image = cv2.rectangle(
                    image, (x, y), (x + w, y + h), (0, 255, 0), 2)
        return image

    def RecChar(image):
        pre_worked = MyTesseract.PreWork(image)
        h, w = pre_worked.shape
        boxes = pytesseract.image_to_boxes(pre_worked)
        for b in boxes.splitlines():
            b = b.split(' ')
            img = cv2.rectangle(
                image, (int(b[1]), h - int(b[2])), (int(b[3]), h - int(b[4])), (0, 255, 0), 2)
        return img

    def PreWork(image):
        gray = MyTesseract.get_grayscale(image)
        a = MyTesseract.thresholding(gray)
        return a

    def Recognize(image):
        return pytesseract.image_to_string(image)
