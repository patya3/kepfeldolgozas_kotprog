import cv2
import numpy as np

def mask_line(line, default, green):
    lowerBound = np.array([0, 255, 0])
    upperBound = np.array([0, 255, 0])

    # Compute mask (roi) from ranges in green lined img
    mask = cv2.inRange(green, lowerBound, upperBound)

    # inverse mask to crop line from default_image
    inv_mask = cv2.bitwise_not(mask)
    dst_masked = cv2.bitwise_and(cv2.cvtColor(inv_mask, cv2.COLOR_GRAY2BGR), default)

    # add line image with black background and default image with the specific line black
    return cv2.bitwise_or(dst_masked, line)