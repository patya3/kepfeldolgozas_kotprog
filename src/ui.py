import PyQt5.QtWidgets as widgets
from PyQt5.uic import loadUi
from os import getcwd
import cv2
import numpy as np
from math import floor
from tesseract_basic import MyTesseract
from character_detection import drawBoundingBox
from masking import mask_line


class TextExtractor(widgets.QMainWindow):
    """Dialog window for TextExtractor application."""

    DEFAULT_COLOR = (168, 164, 50)

    def __init__(self):
        super(TextExtractor, self).__init__()

        self.initVariables()

        # load UI
        loadUi('./src/mainwindow.ui', self)
        self.setWindowTitle('TextExtractor')

        self.initHandlers()

    def initVariables(self):
        self.filename = ''                      # selected filename
        self.points = []                        # points for croping
        self.image_preview = None               # image for preview
        self.image_default = None               # the original image
        self.image_gray = None                  # grayscale image
        self.struct_kernel = [15, 2]            # kernel for dilatiation
        self.text = ''                          # text extracted by tesseract
        self.tesseract_image = None             # tesseract made image
        self.image_final = None                 # final image made by the program with boundinf boxes
        self.image_done = False                 # is the image processing is finished

    def initHandlers(self):
        self.browseButton.clicked.connect(self.onOpenFileDialog)
        self.buttonBox.button(widgets.QDialogButtonBox.Ok).clicked.connect(self.onOpenImage)
        self.buttonBox.button(widgets.QDialogButtonBox.Reset).clicked.connect(self.onResetValues)
        self.buttonBox.button(widgets.QDialogButtonBox.Save).clicked.connect(self.onSaveImage)
        self.cropImageButton.clicked.connect(self.onCropImage)
        self.thresholdButton.clicked.connect(self.onCalcBoundingBoxes)
        self.kernelLabel.setText('Current: (' + str(self.kernelWidthSlider.value()) + ', ' + str(self.kernelHeightSlider.value()) + ')')
        self.kernelWidthSlider.valueChanged.connect(self.onKernelWidthChanged)
        self.kernelHeightSlider.valueChanged.connect(self.onKernelHeightChanged)
        self.teseractButton.clicked.connect(self.tesseract)

        if self.isWindowExists():
            self.thresholdButton.enabled(True)

    def onOpenFileDialog(self):
        """Open FileDialog window. And set self.filename with the selected value."""
        self.onResetValues()
        self.filename = widgets.QFileDialog.getOpenFileName(self, 'Select an image file.', getcwd(), 'Images(*.png *.jpg)')[0]
        self.fileLineEdit.setText(self.filename)
        self.points = []

    def onOpenImage(self):
        """ Opens Image in a cv2 window. """
        if self.filename == '':
            widgets.QMessageBox.warning(self, 'Warning', 'No image selected.', buttons=widgets.QMessageBox.Ok)
            return
        self.image_default = cv2.imread(self.filename, cv2.IMREAD_COLOR)
        try:
            self.image_preview = self.image_default.copy()
        except Exception:
            widgets.QMessageBox.critical(self, 'Error', 'Image path container unexpected characters.', buttons=widgets.QMessageBox.Ok)
        self.thresholdButton.setEnabled(True)
        cv2.namedWindow('Image', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Image', self.calcWidthAndHeight(800, self.image_preview))
        if self.cropImageCheckBox.isChecked():
            cv2.setMouseCallback('Image', self.onImageClick)
        cv2.imshow('Image', self.image_preview)
        cv2.waitKey()
        cv2.destroyAllWindows()

    def onResetValues(self):
        """ Listener on Reset button. Reset all values to default. """
        cv2.destroyAllWindows()
        self.fileLineEdit.clear()
        self.initVariables()
        self.cropImageButton.setEnabled(False)
        self.thresholdButton.setEnabled(False)
        self.kernelLabel.setText('Current: (15, 2)')
        self.kernelWidthSlider.setValue(15)
        self.kernelHeightSlider.setValue(2)

    def onCropImage(self):
        """ Crop Image button listener. Crop image and flatten it. """
        self.image_default = self.transform_image_by_points(self.points, self.image_default)
        self.points = []
        self.image_preview = self.image_default.copy()
        cv2.imshow('Image', self.image_preview)

    def onThreshChanged(self, value):
        """ Change threshold values. """

        self.image_preview = cv2.adaptiveThreshold(self.image_gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, value, -1)
        cv2.imshow('Image', self.image_preview)

    def onKernelWidthChanged(self, value):
        self.struct_kernel[0] = value
        self.kernelLabel.setText('Current: (' + str(value) + ', ' + str(self.kernelHeightSlider.value()) + ')')

    def onKernelHeightChanged(self, value):
        self.struct_kernel[1] = value
        self.kernelLabel.setText('Current: (' + str(self.kernelWidthSlider.value()) + ', ' + str(value) + ')')

    def onCalcBoundingBoxes(self):

        # convert to gray image and save it
        self.image_gray = cv2.cvtColor(self.image_preview, cv2.COLOR_BGR2GRAY)

        # threshold image_gray
        _, thresh = cv2.threshold(
            self.image_gray, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)

        # crop image, leave only parts which have content
        non_zeroes = cv2.findNonZero(thresh)
        min_x, max_x = np.min(non_zeroes[:,:,0]), np.max(non_zeroes[:,:,0])
        min_y, max_y = np.min(non_zeroes[:,:,1]), np.max(non_zeroes[:,:,1])
        cropped_thresh = thresh[min_y:max_y+1, min_x:max_x+1]


        # create kernel based on the user given params
        rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, tuple(self.struct_kernel))
        dilation = cv2.dilate(cropped_thresh, rect_kernel, iterations=4)

        # find contours on the dilated image
        contours, _ = cv2.findContours(
            dilation, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        lines = self.image_default.copy()
        min_area_rects = [cv2.minAreaRect(x) for x in contours]
        avg_area_size_threshold = np.average([np.prod(x[1]) for x in min_area_rects])/30
        avg_area_height_threshold = np.average([x[1][1] for x in min_area_rects])/2
        for index in range(0, len(contours)):
            # current contour smallest boundig rect
            rect_center, rect_size, rect_angle = min_area_rects[index]

            if rect_size[0]*rect_size[1] < avg_area_size_threshold and rect_size[1] < avg_area_height_threshold:
                continue
            
            # crate bounding box on the cropped image
            box = cv2.boxPoints((rect_center, rect_size, rect_angle))
            box = np.int0(box)
            
            # transform boundig box back to the default coordinate system
            box = np.add(box[:], [min_x, min_y])

            # draw contours on the image
            cv2.drawContours(lines, [box], 0, (0, 0, 255), 2, cv2.LINE_4)

            # crop current line from the default image
            crop_image = self.transform_image_by_points(box, self.image_default, crop=True)
            
            # draw boundingboxes on the line
            crop_image = drawBoundingBox(crop_image)
            # perspective transform the line back to its original coordinate system
            crop_image, warped_green = self.transform_image_by_points(box, crop_image, crop=False)

            # replace current line with the line containing bounding boxes
            if self.image_final is None:
                self.image_final = mask_line(crop_image, self.image_default, warped_green)
            else:
                self.image_final = mask_line(crop_image, self.image_final, warped_green)
        
        self.image_done = True
        cv2.namedWindow('Dilation', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Dilation', self.calcWidthAndHeight(800, dilation))
        cv2.namedWindow('Lines', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Lines', self.calcWidthAndHeight(800, lines))
        cv2.imshow('Lines', lines)
        cv2.imshow('Dilation', dilation)
        cv2.imshow('Image' , self.image_final)
        cv2.waitKey(0)

    def onImageClick(self, event, x, y, flags, params):
        """ Register a click listener for the cv2 image window. And draw points and lines on image. """
        if event == cv2.EVENT_LBUTTONDOWN and len(self.points) < 4:
            self.points.append((x, y))
            cv2.circle(self.image_preview,self.points[-1], 4, TextExtractor.DEFAULT_COLOR, 12)
            if len(self.points) >= 2:
                cv2.line(self.image_preview, self.points[-2], self.points[-1], TextExtractor.DEFAULT_COLOR, 6)
            if len(self.points) == 4:
                cv2.line(self.image_preview, self.points[-1], self.points[0], TextExtractor.DEFAULT_COLOR, 6)
            cv2.imshow('Image', self.image_preview)
        else:
            self.cropImageButton.setEnabled(self.cropImageCheckBox.isChecked() and len(self.points) == 4)

    def onSaveImage(self):
        if self.image_done:
            try:
                cv2.imwrite('result.png', self.image_final)
                widgets.QMessageBox.information(self, 'Saved', 'Image successfully saved to result.png', buttons=widgets.QMessageBox.Ok)
            except Exception:
                widgets.QMessageBox.critical(self, 'Error', 'Error while saving the image.', buttons=widgets.QMessageBox.Ok)
        else:
            widgets.QMessageBox.warning(self, 'Warning', 'The image is not ready yet.', buttons=widgets.QMessageBox.Ok)

    def show_dialog(self):
        textDialog = TextDialog(self)
        textDialog.textEdit.setText(self.text)
        return textDialog.exec()

    def tesseract(self):
        self.tesseract_image = None
        try:
            self.text = MyTesseract.Recognize(self.image_default)
            result = self.show_dialog()
            if result:
                self.tesseract_image = MyTesseract.RecChar(self.image_default.copy())
                cv2.imshow('Tesseract', self.tesseract_image)
        except Exception:
            widgets.QMessageBox.critical(self, 'Error', 'Tesseract not found.', buttons=widgets.QMessageBox.Ok)
            return

    def calcWidthAndHeight(self, long_side_size, image):
        """ Calc new image width and height. """
        height, width = image.shape[:2]
        if height > width:
            width = floor(width * (long_side_size/height))
            height = long_side_size
        else:
            height = floor(height * (long_side_size/width))
            width = long_side_size
        return (width, height)

    def isWindowExists(self, window_name='Image'):
        """ Check window exists. """
        return cv2.getWindowProperty(window_name, cv2.WND_PROP_VISIBLE) >= 1

    def crop_image(self, image, points):
        """ Crop image by the given points. """
        x = points[:,0]
        y = points[:,1]
        top_left_x = min(x)
        top_left_y = min(y)
        bot_right_x = max(x)
        bot_right_y = max(y)
        return image[top_left_y:bot_right_y+1, top_left_x:bot_right_x+1]


    def transform_image_by_points(self, points, image, crop=True):
        """ Transform (flatten) image by the given points. """
        pts = np.array(points, dtype='float32')

        def order_points(pts):
            """ Orders the given points from the top-left clockwise to the bottom-left. """
            # initialzie a list of coordinates that will be ordered
            # such that the first entry in the list is the top-left,
            # the second entry is the top-right, the third is the
            # bottom-right, and the fourth is the bottom-left
            rect = np.zeros((4, 2), dtype="float32")
            # the top-left point will have the smallest sum, whereas
            # the bottom-right point will have the largest sum
            s = pts.sum(axis=1)
            rect[0] = pts[np.argmin(s)]
            rect[2] = pts[np.argmax(s)]
            # now, compute the difference between the points, the
            # top-right point will have the smallest difference,
            # whereas the bottom-left will have the largest difference
            diff = np.diff(pts, axis=1)
            rect[1] = pts[np.argmin(diff)]
            rect[3] = pts[np.argmax(diff)]
            # return the ordered coordinates
            return rect

        # obtain a consistent order of the points and unpack them
        # individually
        rect = order_points(pts)
        (tl, tr, br, bl) = rect
        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordiates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))
        # compute the height of the new image, which will be the
        # maximum distance between the top-right and bottom-right
        # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))
        # now that we have the dimensions of the new image, construct
        # the set of destination points to obtain a "birds eye view",
        # (i.e. top-down view) of the image, again specifying points
        # in the top-left, top-right, bottom-right, and bottom-left
        # order
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype="float32")
        # compute the perspective transform matrix and then apply it
        M = cv2.getPerspectiveTransform(rect, dst)
        M2 = cv2.getPerspectiveTransform(dst, rect)
        if crop:
            warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
            return warped
        else:
            # line transformed back to its default position
            warped_full = cv2.warpPerspective(image, M2, (self.image_default.shape[1], self.image_default.shape[0]))

            warped_line = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
            warped_green = warped_line.copy()
            warped_green[:] = (0, 255, 0)
            # image with green line on the place of the current line
            warped_green = cv2.warpPerspective(warped_green, M2, (self.image_default.shape[1], self.image_default.shape[0]))
            # return the warped image
            return warped_full, warped_green


class TextDialog(widgets.QDialog):
    """Employee dialog."""

    def __init__(self, parent=None):
        super().__init__(parent)
        # Load the dialog's GUI
        loadUi("./src/dialog.ui", self)