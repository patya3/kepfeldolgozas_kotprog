# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['src\\main.py', 'src\\ui.py', 'src\\tesseract_basic.py', 'src\\masking.py', 'src\\character_detection.py'],
             pathex=['I:\\Programing\\msc\\sz2\\kepfeldolgozas_haladoknak\\gyakorlat\\project-git\\kepfeldolgozas_kotprog'],
             binaries=[],
             datas=[('src\\mainwindow.ui','src'), ('src\\dialog.ui', 'src')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='main',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='main')
